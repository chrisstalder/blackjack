/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.blackjack;

import com.mycompany.blackjack.dto.User;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Stalder
 */
@Controller
public class HomeController {
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(Map model){
        Integer playerCash = 100;
        Integer initialPlayerTotal = 0;
        Integer initialDealerTotal = 0;
        User player = new User();
        player.setId(2);
        player.setName("Test");
        player.setPassword("test");
        player.setPlayerCash(100);
        player.setWins(0);
        player.setLosses(0);
        model.put("player", player);
        model.put("playerCash", playerCash);
        model.put("playerTotal",initialPlayerTotal);
        model.put("dealerTotal",initialDealerTotal);
        return "home";
    }
}
