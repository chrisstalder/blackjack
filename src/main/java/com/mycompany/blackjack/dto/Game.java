/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.blackjack.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Stalder
 */
public class Game {

    private Integer id;
    private Integer bet;
    private Integer playerTotal;
    private Integer dealerTotal;
    private User player;
    private Card playerCard1;
    private Card playerCard2;
    private Card playerNextCard;
    private Card dealerCard1;
    private Card dealerCard2;
    private Card dealerNextCard;

    public Card getPlayerCard1() {
        return playerCard1;
    }

    public void setPlayerCard1(Card playerCard1) {
        this.playerCard1 = playerCard1;
    }

    public Card getPlayerCard2() {
        return playerCard2;
    }

    public void setPlayerCard2(Card playerCard2) {
        this.playerCard2 = playerCard2;
    }

    public Card getPlayerNextCard() {
        return playerNextCard;
    }

    public void setPlayerNextCard(Card playerNextCard) {
        this.playerNextCard = playerNextCard;
    }

    public Card getDealerCard1() {
        return dealerCard1;
    }

    public void setDealerCard1(Card dealerCard1) {
        this.dealerCard1 = dealerCard1;
    }

    public Card getDealerCard2() {
        return dealerCard2;
    }

    public void setDealerCard2(Card dealerCard2) {
        this.dealerCard2 = dealerCard2;
    }

    public Card getDealerNextCard() {
        return dealerNextCard;
    }

    public void setDealerNextCard(Card dealerNextCard) {
        this.dealerNextCard = dealerNextCard;
    }
    

    public Integer getBet() {
        return bet;
    }

    public void setBet(Integer bet) {
        this.bet = bet;
    }

    public User getPlayer() {
        return player;
    }

    public void setPlayer(User player) {
        this.player = player;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerTotal() {
        return playerTotal;
    }

    public void setPlayerTotal(Integer playerTotal) {
        this.playerTotal = playerTotal;
    }

    public Integer getDealerTotal() {
        return dealerTotal;
    }

    public void setDealerTotal(Integer dealerTotal) {
        this.dealerTotal = dealerTotal;
    }

}
