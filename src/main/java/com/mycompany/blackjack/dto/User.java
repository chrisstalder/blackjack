/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.blackjack.dto;

import java.util.Date;

/**
 *
 * @author apprentice
 */
public class User {
    private int id;
    
//    @NotEmpty(message="You must supply a name")
    private String name;

//    @NotEmpty(message="You must supply a password")
    private String password;
    
    private Integer playerCash;
    
    private Integer wins;
    
    private Integer losses;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPlayerCash() {
        return playerCash;
    }

    public void setPlayerCash(Integer playerCash) {
        this.playerCash = playerCash;
    }

    public Integer getWins() {
        return wins;
    }

    public void setWins(Integer wins) {
        this.wins = wins;
    }

    public Integer getLosses() {
        return losses;
    }

    public void setLosses(Integer losses) {
        this.losses = losses;
    }
   
}
