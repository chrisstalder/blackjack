/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.blackjack.dao;

import com.mycompany.blackjack.dto.Game;
import com.mycompany.blackjack.dto.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Stalder
 */
public class GameDaoImpl implements GameDao {

    private static final String SQL_INSERT_GAME= "INSERT INTO game (player_total, dealer_total, user_id) VALUES (?, ?, ?)";
    private static final String SQL_UPDATE_GAME = "UPDATE game SET player_total = ?, dealer_total = ?, user_id = ? WHERE id = ?";
    private static final String SQL_DELETE_GAME = "DELETE FROM game WHERE id = ?";
    private static final String SQL_SELECT_GAME = "SELECT * FROM game WHERE id = ?";
    private static final String SQL_GET_GAME_LIST = "SELECT * FROM game";
    private JdbcTemplate jdbcTemplate;

    public GameDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Game create(Game game) {
        jdbcTemplate.update(SQL_INSERT_GAME,
                game.getPlayerTotal(),
                game.getDealerTotal(),
                game.getPlayer().getId()
        );

        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        game.setId(id);

        return game;
    }

    @Override
    public void update(Game game) {
        jdbcTemplate.update(SQL_UPDATE_GAME,
                game.getPlayerTotal(),
                game.getDealerTotal(),
                game.getPlayer().getId(),
                game.getId()
        );
    }

    @Override
    public Game get(Integer id) {
        return jdbcTemplate.queryForObject(SQL_SELECT_GAME, new GameMapper(), id);
    }

    @Override
    public void delete(Game game) {
        jdbcTemplate.update(SQL_DELETE_GAME, game.getId());
    }

    @Override
    public List<Game> list() {
        return jdbcTemplate.query(SQL_GET_GAME_LIST, new GameMapper());
    }

    private static final class GameMapper implements RowMapper<Game> {

        @Override
        public Game mapRow(ResultSet rs, int i) throws SQLException {
            Game game = new Game();

            game.setId(rs.getInt("id"));
            game.setPlayerTotal(rs.getInt("player_total"));
            game.setDealerTotal(rs.getInt("dealer_total"));
            User player = new User();
            player.setId(rs.getInt("user_id"));
            game.setPlayer(player);
          

            return game;
        }

    }
}
