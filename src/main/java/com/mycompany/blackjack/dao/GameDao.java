package com.mycompany.blackjack.dao;

import com.mycompany.blackjack.dto.Game;
import java.util.List;


public interface GameDao {

    public Game create(Game game);
    public void update(Game game);
    public Game get(Integer id);
    public void delete(Game game);
    public List<Game> list();
}