/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.blackjack.dao;

import com.mycompany.blackjack.dto.User;
import com.mycompany.blackjack.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class UserDaoImpl implements UserDao {

    private static final String SQL_INSERT_USER = "INSERT INTO user (name, password, player_cash, wins, losses) VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_USER = "UPDATE user SET name = ?, password = ?, player_cash = ?, wins = ?, losses = ? WHERE id = ?";
    private static final String SQL_SOFT_DELETE_USER = "UPDATE user SET enabled = 0 WHERE id = ?";
    private static final String SQL_DELETE_USER = "DELETE FROM user WHERE id = ?";
    private static final String SQL_GET_USER = "SELECT * FROM user WHERE id =?";
    private static final String SQL_GET_USER_LIST = "SELECT * FROM user WHERE enabled = 1";

    private JdbcTemplate jdbcTemplate;

    public UserDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public User create(User user) {

        if (user == null) {
            return null;
        }

        jdbcTemplate.update(SQL_INSERT_USER,
                user.getName(),
                user.getPassword(),
                user.getPlayerCash(),
                user.getWins(),
                user.getLosses());

        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        user.setId(id);
        
        return user;

    }

    @Override
    public User get(int id) {

        try {
            return jdbcTemplate.queryForObject(SQL_GET_USER, new UserMapper(), id);
        } catch (org.springframework.dao.EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public void update(User user) {

        if (user == null) {
            return;
        }

        if (user.getId() > 0) {

            jdbcTemplate.update(SQL_INSERT_USER,
                user.getName(),
                user.getPassword(),
                user.getPlayerCash(),
                user.getWins(),
                user.getLosses(),
                user.getId());
        }
    }

    @Override
    public void delete(User user) {
        if (user == null) {
            return;
        }
        jdbcTemplate.update(SQL_DELETE_USER, user.getId());
    }

    @Override
    public List<User> list() {
        return jdbcTemplate.query(SQL_GET_USER_LIST, new UserMapper());
    }

    private static final class UserMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {

            User user = new User();

            user.setId(rs.getInt("id"));
            user.setName(rs.getString("name"));
            user.setPassword(rs.getString("password"));
            user.setPlayerCash(rs.getInt("player_cash"));
            user.setWins(rs.getInt("wins"));
            user.setLosses(rs.getInt("losses"));
            return user;
        }
    }

}
