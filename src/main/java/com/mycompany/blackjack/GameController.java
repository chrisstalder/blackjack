/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.blackjack;

import com.mycompany.blackjack.dao.GameDao;
import com.mycompany.blackjack.dao.UserDao;
import com.mycompany.blackjack.dto.Card;
import com.mycompany.blackjack.dto.DeckOfCards;
import com.mycompany.blackjack.dto.Game;
import com.mycompany.blackjack.dto.GameCommand;
import com.mycompany.blackjack.dto.User;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Stalder
 */
@Controller
@RequestMapping(value = "/play")
public class GameController {

    private DeckOfCards doc = new DeckOfCards();
    private List<Card> cards = doc.DeckOfCards();
    private Scanner keyboard = new Scanner(System.in);
    private GameDao gameDao;
    private UserDao userDao;

    @Inject
    public GameController(GameDao gameDao, UserDao userDao) {
        this.gameDao = gameDao;
        this.userDao = userDao;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public Game start(@RequestBody GameCommand gameCommand) {
        Game game = new Game();
        game.setPlayerTotal(gameCommand.getPlayerTotal());
        game.setDealerTotal(gameCommand.getDealerTotal());
        game.setBet(gameCommand.getBet());
        User player = new User();
        player.setId(gameCommand.getPlayerId());
        game.setPlayer(player);

        Card playerCard1 = doc.dealCard(cards);
        Card playerCard2 = doc.dealCard(cards);
        Card dealerCard1 = doc.dealCard(cards);
        Card dealerCard2 = doc.dealCard(cards);
        
        game.setPlayerCard1(playerCard1);
        game.setPlayerCard2(playerCard2);
        game.setDealerCard1(dealerCard1);
        game.setDealerCard2(dealerCard2);
        
        int newTotal = 0;
        int playerTotal = doc.cardTotal(playerCard1, playerCard2);
        int dealerTotal = doc.cardTotal(dealerCard1, dealerCard2);
        game.setPlayerTotal(playerTotal);
        game.setDealerTotal(dealerTotal);

        return gameDao.create(game);

    }

    public void run() {

        boolean playAgain = true;
        int playerCash = 100;
        while (playAgain) {

            int bet = 0;

            String betResponse = "";
            boolean isValid = false;
            boolean anotherHit = true;
            boolean dealerAnotherHit = true;
            Card playerCard1 = doc.dealCard(cards);
            Card playerCard2 = doc.dealCard(cards);
            Card dealerCard1 = doc.dealCard(cards);
            Card dealerCard2 = doc.dealCard(cards);
            int newTotal = 0;
            int playerTotal = doc.cardTotal(playerCard1, playerCard2);
            int dealerTotal = doc.cardTotal(dealerCard1, dealerCard2);
            boolean cardTotalValidation = isValid(playerTotal);
            boolean dealerCardTotalValidation = isValid(dealerTotal);
            boolean answer = true;
            boolean dealerAnswer = true;

            System.out.println("You have $" + playerCash);
            System.out.println("Dealer draws " + dealerCard1.getName());
            System.out.println("You drew " + playerCard1.getName());
            System.out.println("Would you like to place a bet?");

            while (!isValid) {
//                    betResponse = keyboard.next();
                if (betResponse.matches("\\d+")) {
                    isValid = false;
                } else {
                    betResponse.toLowerCase();
                    isValid = true;
                }
            }
            isValid = false;

            if ("yes".equals(betResponse)) {
                System.out.println("How much would you like to bet?");
                while (!isValid) {
                    String placeBet = keyboard.next();
                    try {
                        bet = Integer.parseInt(placeBet);
                        isValid = true;
                    } catch (Exception e) {
                        System.out.println("Please enter a valid bet.");
                    }
                }
                System.out.println("Dealer draws " + dealerCard2.getName());
                System.out.println("You are dealt " + playerCard2.getName());
                System.out.println("Your total is " + playerTotal);

                while (anotherHit) {

                    System.out.println("Do you want a hit?");
                    String response = keyboard.next();
                    answer = answerValidation(response);

                    while (cardTotalValidation && answer) {

                        Card nextCard = doc.dealCard(cards);
                        newTotal = doc.hitTotal(playerTotal, nextCard);
                        cardTotalValidation = isValid(newTotal);
                        if (cardTotalValidation) {
                            playerTotal = newTotal;
                            System.out.println("You drew " + nextCard.getName());
                            System.out.println("Your total is " + newTotal);

                            System.out.println("Do you want another hit?");
                            response = keyboard.next();
                            answer = answerValidation(response);
                        } else if (cardTotalValidation == false) {
                            System.out.println("You drew " + nextCard.getName());
                            System.out.println("You went over! Dealer Wins!");
                            playerCash -= bet;
                            anotherHit = false;
                        }
                    }
                    if (answer == false) {
                        anotherHit = false;
                    }
                }

                if (answer == false && cardTotalValidation) {

                    while (dealerAnotherHit) {

                        dealerAnswer = dealerHit(dealerTotal, playerTotal);

                        while (dealerCardTotalValidation && dealerAnswer) {

                            Card dealerNextCard = doc.dealCard(cards);
                            int dealerNewTotal = doc.hitTotal(dealerTotal, dealerNextCard);
                            dealerCardTotalValidation = isValid(dealerNewTotal);
                            if (dealerCardTotalValidation) {
                                System.out.println("Dealer will hit.");
                                dealerTotal = dealerNewTotal;
                                System.out.println("Dealer drew " + dealerNextCard.getName());
                                System.out.println("Dealer's total is " + dealerNewTotal);

                                dealerAnswer = dealerHit(dealerTotal, playerTotal);
                            } else if (dealerCardTotalValidation == false) {
                                System.out.println("Dealer drew " + dealerNextCard.getName());
                                System.out.println("Dealer busts!");
                                dealerAnotherHit = false;
                                playerCash += bet;
                            }

                        }

                        if (dealerAnswer == false) {
                            dealerAnotherHit = false;
                        }
                    }
                    if (playerTotal > dealerTotal && playerTotal <= 21) {
                        System.out.println("You win!");
                    } else if (playerTotal < dealerTotal && dealerTotal <= 21) {
                        System.out.println("The dealer wins!");
                        playerCash -= bet;
                    } else if (playerTotal == dealerTotal && playerTotal <= 21) {
                        System.out.println("You tied the dealer.");
                    }
                }
            } else {
                System.out.println("Player folds. Dealer wins.");
                playerCash -= bet;
            }
            if (playerCash > 0) {
                System.out.println("Would you like to play another hand?");
                String anotherHand = keyboard.next().toLowerCase();
                if ("yes".equals(anotherHand)) {
                    playAgain = true;
                } else if ("no".equals(anotherHand)) {
                    playAgain = false;
                }
            } else {
                System.out.println("You don't have any money. Come back when you aren't broke!");
                playAgain = false;
            }
        }
    }

    public boolean isValid(int cardTotal) {
        boolean isValid;
        isValid = cardTotal <= 21;

        return isValid;
    }

    public boolean answerValidation(String response) {

        boolean isValid = false;
        boolean whileLoop = false;

        while (!whileLoop) {
            if ("yes".equals(response)) {
                isValid = true;
                whileLoop = true;
            } else if ("no".equals(response)) {
                isValid = false;
                whileLoop = true;
            } else {
                System.out.println("I couldn't quite hear you. Do you want a hit?");
                response = keyboard.next();
                whileLoop = false;
            }
        }

        return isValid;
    }

    public boolean dealerHit(int dealerTotal, int playerTotal) {
        boolean shouldHit;

        if (dealerTotal <= 16 || dealerTotal < playerTotal) {
            shouldHit = true;
        } else {
            shouldHit = false;
        }

        return shouldHit;
    }
}
