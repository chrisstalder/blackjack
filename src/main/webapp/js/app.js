/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#play').on('click', function (e) {
        alert('works');
        e.preventDefault();
        var gameData = JSON.stringify({
            bet: $('#bet').val(),
            playerTotal: $('#playerTotal').val(),
            dealerTotal: $('#dealerTotal').val(),
            player: $('#player').val()
        });
        
        $.ajax({
            url: contextRoot + "/play/",
            type: "POST",
            data: gameData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                alert('success');
                console.log(data);

                var response = build(data);
                $('#myDiv').append($(response));

                $('#bet').val('');

            },
            error: function (data, status) {
                alert('failure');
                console.log(status);
                console.log(data);
//                var errors = data.responseJSON.errors;
//                
//                $.each(errors, function(index, error) {
//                   
//                    $('#add-address-validation-errors').append(error.message + "<br />");
//                    
//                });
            }

        });

    });

    $('#showAddressModal').on('show.bs.modal', function (e) {
        
        var link = $(e.relatedTarget);
        
        var addressId = link.data('address-id');
        
        $.ajax({
            
            url: contextRoot + '/address/' + addressId,
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr){
                xhr.setRequestHeader('Accept', 'application/json');
            },
            success: function(data, status){
                $('#address-first-name').text(data.firstName);
                $('#address-last-name').text(data.lastName);
                $('#address-street-number').text(data.streetNumber);
                $('#address-street-name').text(data.streetName);
                $('#address-city').text(data.city);
                $('#address-state').text(data.state);
                $('#address-zip').text(data.zip);
            },
            error: function(data, status){
                alert("error");
            }
            
        });
        
    });
    
    $('#editAddressModal').on('show.bs.modal', function (e) {
        
        var link = $(e.relatedTarget);
        
        var addressId = link.data('address-id');
        
        $.ajax({
            
            url: contextRoot + '/address/' + addressId,
            type: 'GET',
            dataType: 'json',
            beforeSend: function(xhr){
                xhr.setRequestHeader('Accept', 'application/json');
            },
            success: function(data, status){
                $('#edit-address-first-name').val(data.firstName);
                $('#edit-address-last-name').val(data.lastName);
                $('#edit-address-street-number').val(data.streetNumber);
                $('#edit-address-street-name').val(data.streetName);
                $('#edit-address-city').val(data.city);
                $('#edit-address-state').val(data.state);
                $('#edit-address-zip').val(data.zip);
                $('#edit-id').val(data.id);
            },
            error: function(data, status){
                alert("error");
            }
            
        });
        
    });
    
    $('#edit-address-button').on('click', function(e){
        $('#edit-address-validation-errors').empty();
        var addressData = JSON.stringify({
            firstName: $('#edit-address-first-name').val(),
            lastName: $('#edit-address-last-name').val(),
            streetNumber: $('#edit-address-street-number').val(),
            streetName: $('#edit-address-street-name').val(),
            city: $('#edit-address-city').val(),
            state: $('#edit-address-state').val(),
            zip: $('#edit-address-zip').val(),
            id: $('#edit-id').val()

        });

        $.ajax({
            url: contextRoot + "/address/",
            type: "PUT",
            data: addressData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {

                $('#editAddressModal').modal('hide');
                var tableRow = buildContactRow(data);
                
                $('#address-row-'+ data.id).replaceWith($(tableRow));
            },
            error: function (data, status) {
                var errors = data.responseJSON.errors;
                
                $.each(errors, function(index, error) {
                   
                    $('#edit-address-validation-errors').append(error.message + "<br />");
                    
                });
            }

        });
        
    });
    
    $(document).on('click', '.delete-link', function(e){
        
        e.preventDefault();
        
        var addressId = $(e.target).data('address-id');
        
        $.ajax({
            url: contextRoot + '/address/' + addressId,
            type: 'DELETE',
            success:function (data, status){
                $('#address-row-' + addressId).remove();
            },
            error: function(data,status){
                
            }
        });
        
    });
    
    
    
    function build(data) {

        return "<p>You are dealt " + data.playerCard1.name + " and " + data.playerCard1.name + "</p>\n\
                <p>Dealer draws " + data.dealerCard1.name + " and " + data.dealerCard2.name + "</p>\n\
                <p>Your total is " + data.playerTotal + "</p>\n\
                <p>Do you want another hit?</p>\n\
                <input id='hit' type='submit' value='hit' class='btn btn-group-lg center-block'/>";

    }
});