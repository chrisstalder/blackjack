<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>BlackJack</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
<!--        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">-->

    </head>
    <body>
        <div class="container">
            <h1>BlackJack</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/login/">Login</a></li>
                </ul>    
            </div>

            <div class="row">

                <div class="col-md-12">
                    <div class="row">
                        
                        <div class="col-md-4">
                            <form method="POST" class="form-horizontal">
                                <input name="playerTotal" type="hidden" value="${playerTotal}" id="playerTotal"/>
                                <input name="dealerTotal" type="hidden" value="${dealerTotal}" id="dealerTotal"/>
                                <input name="playerId" type="hidden" value="${player.id}" id="player"/>
                                <div class="form-group">
                                    <label for="bet" class="col-md-4 control-label">Bet:</label>
                                    <div class="col-md-8">
                                        <input name="bet" type="text" id="bet" class="form-control"/>
                                    </div>
                                </div>

                                <input id="play" type="submit" value="play" class="btn btn-group-lg center-block"/><br>
                            </form><br>
                            <div id="game-validation-errors" class="pull-right"></div>
                        </div>
                        <div class="col-md-4" id="myDiv"></div>
                        <div class="col-md-4"></div>
                    </div>

                </div>

            </div>

        </div>

        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/app.js"></script>
    </body>
</html>

