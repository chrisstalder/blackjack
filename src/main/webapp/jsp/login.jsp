<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%--<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>--%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/homeLogin.css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <form:form action="${pageContext.request.contextPath}/j_spring_security_check" commandName="user" method="POST">
                <div class="login-box">
                    <div class="box-header">
                        <h2>Log In</h2>
                    </div>
                    <label for="username">Username</label><br/>
                    <input type="text" name="username" id="login-username"/><br/>
                    <label for="password">Password</label><br/>
                    <input type="password" name="password" id="login-password"/><br/>
                    <input type="submit" value="Login"/>
                    <br/>
                    <a href="${pageContext.request.contextPath}/createAccount"><p class="small">Create an Account</p></a>

                    <c:if test="${loginError == 1}">
                        <div>Error logging in</div>
                    </c:if>


                </div>
            </form:form>              
        </div>
        <%--<%@ include file="footer.jsp" %>--%>


        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/custom.js"></script>
    </body>
</html>

